$(function(){
    //判断是否是手机
    var mobile_flag = isMobile();
    console.log(mobile_flag)
    if(mobile_flag) {
      $(".phone").show()
      $(".pc").hide()
      dynamicLoadCss("phone/index.css")
      dynamicLoadJs("phone/index.js")
    // document.writeln("<script src=\"phone/index.js\"></script>");
    } else {
      $(".pc").show()
      $(".phone").hide()
      dynamicLoadCss("pc/index.css")
      dynamicLoadJs("pc/index.js")
      // document.writeln("<script src='pc/index.js'></script>");
     }
  })
   //加载外部css文件
	function dynamicLoadCss(url) {
		var head = document.getElementsByTagName('head')[0];
		var link = document.createElement('link');
		link.type='text/css';
		link.rel = 'stylesheet';
		link.href = url;
		head.appendChild(link);
  }
  //加载外部js文件
	function dynamicLoadJs(url) {
		var body = document.getElementsByTagName('body')[0];
		var script = document.createElement('script');
		script.type='text/jajvascript';
		script.src = url;
		body.appendChild(script);
	}
  function isMobile() {
    var userAgentInfo = navigator.userAgent;
    var mobileAgents = [ "Android", "iPhone", "SymbianOS", "Windows Phone", "iPad","iPod"];
    var mobile_flag = false;
    //根据userAgent判断是否是手机
    for (var v = 0; v < mobileAgents.length; v++) {
        if (userAgentInfo.indexOf(mobileAgents[v]) > 0) {
            mobile_flag = true;
            break;
        }
    }
    var screen_width = window.screen.width;
    var screen_height = window.screen.height;   
  
     //根据屏幕分辨率判断是否是手机
     if(screen_width < 500 && screen_height < 800){
         mobile_flag = true;
    }
    return mobile_flag;
  }