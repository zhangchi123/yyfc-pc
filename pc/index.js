if (location.hostname == 'www.ruiwenjy.com' || location.hostname == 'www.yinyuefengchao.com') {
    HOST = "https://admin.yyfcdrm.com";
} else {
    HOST = "https://cms-ubtest-admin.ruiwenjy.com";
}
var act_qd_str = getQueryVariable('act_qd_str');
//来自谷歌搜索的认为是海外用户
var isFromGoogle = document.referrer&&document.referrer.indexOf("google.com")>-1;
var isHaiwai = false;
isHaiwai = act_qd_str&&act_qd_str.indexOf("haiwai")==0 || isFromGoogle ; 
 
if (location.hostname == 'www.ruiwenjy.com') {
    document.getElementById("icp").innerHTML = '辽ICP备17002816号-1'
} else if (location.hostname == 'www.yinyuefengchao.com') {
    document.getElementById("icp").innerHTML = '辽ICP备17002816号-3'
    document.title = '音乐蜂巢官网_少儿在线1对1钢琴课，学琴陪练考级三合一，免费体验'
    var objs = document.getElementsByTagName("meta");
    for (var i = 0; i < objs.length; i++) {
        if (objs[i].name === 'keywords') {
            objs[i].content = '音乐蜂巢,钢琴,电子琴,幼少儿童孩子学钢琴,钢琴启蒙,钢琴家教,0零基础学钢琴,钢琴陪练,钢琴在线学习,钢琴在线陪练,钢琴视频教学,儿童孩子才艺,怎样学练好钢琴,钢琴陪练软件,钢琴学习app,钢琴线上课哪个好,在家学钢琴,孩子学钢琴app,网上钢琴教学陪练,一对一钢琴教学老师陪练,网上钢琴教学,学钢琴入门到精通,钢琴培训,男女孩学钢琴,女儿子学钢琴,琴童,学弹琴,学琴软件,零0基础学钢琴,钢琴app,1对1钢琴,vip快陪练,小叶子熊猫陪练钢琴,练琴,弹琴,电钢琴,手卷钢琴,电子琴,乐器,才艺,音乐启蒙,兴趣班,买什么钢琴,搬琴,声乐,舞蹈班,芭蕾,钢琴怎么学,孩子不学钢琴,宝妈,几岁学钢琴,学钢琴年龄,456789岁,学钢琴的好处,钢琴学费,钢琴家教,北京上海杭州苏州南京沈阳青岛学钢琴培训,theone,AI陪练,快陪练,小叶子陪练,VIP钢琴陪练,少儿编程,ahaschool,画啦啦,美术宝,vipkid,火花思维';
        }
    }
    for (var i = 0; i < objs.length; i++) {
        if (objs[i].name === 'description') {
            objs[i].content = '音乐蜂巢是专注4-9岁少儿在线1对1钢琴教学开创者。五阶课程体系由央音、国音、沈音教授、中法两国知名作曲家和台湾儿童心理学者共同倾力研发，学习效果对标音协、英皇考级水准。';
        }
    }
} else if (location.hostname == 'www.zaixiangangqin.com') {
    document.getElementById("icp").innerHTML = '辽ICP备17002816号-8'
    document.title = '音乐蜂巢-专注4-9岁少儿在线1对1钢琴教学/陪练/考级'
    var objs = document.getElementsByTagName("meta");
    for (var i = 0; i < objs.length; i++) {
        if (objs[i].name === 'keywords') {
            objs[i].content = '钢琴,电子琴,vip钢琴陪练,北京上海深圳广州孩子学钢琴陪练机构多少钱,儿童钢琴陪练,钢琴在线学习陪练,网线上陪练,孩子练钢琴视频,钢琴陪练软件,钢琴学习APP,钢琴线上课哪个好,在家学钢琴陪练,孩子学钢琴陪练哪个app好,音乐蜂巢,练琴,钢琴才艺,孩子兴趣班,在线学钢琴,钢琴在线陪练 ,幼小衔接,幼升小,琴童,在线学钢琴一对一 ,在线陪练 ,多大学钢琴,怎样练好钢琴,学钢琴的好处,线网上学钢琴陪练 ,学钢琴陪练软件 ,熊猫陪练 ,熊猫钢琴, 熊猫钢琴陪练, 熊猫陪练, AI陪练,快陪练,小叶子陪练,VIP钢琴陪练,少儿编程,ahaschool,画啦啦,美术宝,vipkid,火花思维';
        }
    }
    for (var i = 0; i < objs.length; i++) {
        if (objs[i].name === 'description') {
            objs[i].content = '音乐蜂巢将学琴和练琴融入一个APP：趣味动画课堂，高效激发孩子学琴主动性，以专业师资真人1对1为纽带，着重鼓励式引导教学；3x3种AI游戏化智能练琴系统，让零基础孩子轻松学琴、快乐练琴，彻底粉碎传统方式学琴的痛苦。';
        }
    }
} else if (location.hostname == 'www.gangqinpeilian.vip') {
    document.getElementById("icp").innerHTML = '辽ICP备17002816号-5'
    document.title = '音乐蜂巢在线1对1名师钢琴陪练课，对标考级水准学钢琴'
    var objs = document.getElementsByTagName("meta");
    for (var i = 0; i < objs.length; i++) {
        if (objs[i].name === 'keywords') {
            objs[i].content = 'vip钢琴陪练,钢琴,电子琴,北京上海深圳广州孩子学钢琴陪练机构多少钱,儿童钢琴陪练,钢琴在线学习陪练,网线上陪练,孩子练钢琴视频,钢琴陪练软件,钢琴学习APP,钢琴线上课哪个好,在家学钢琴陪练,孩子学钢琴陪练哪个app好,音乐蜂巢,练琴,钢琴才艺,孩子兴趣班,在线学钢琴,钢琴在线陪练 ,幼小衔接,幼升小,琴童,在线学钢琴一对一 ,在线陪练 ,多大学钢琴,怎样练好钢琴,学钢琴的好处,线网上学钢琴陪练 ,学钢琴陪练软件 ,熊猫陪练 ,熊猫钢琴, 熊猫钢琴陪练, 熊猫陪练, AI陪练,快陪练,小叶子陪练,VIP钢琴陪练,少儿编程,ahaschool,画啦啦,美术宝,vipkid,火花思维';
        }
    }
    for (var i = 0; i < objs.length; i++) {
        if (objs[i].name === 'description') {
            objs[i].content = '音乐蜂巢将学琴和练琴融入一个APP：趣味动画课堂，高效激发孩子学琴主动性，以专业师资真人1对1为纽带，着重鼓励式引导教学；3x3种AI游戏化智能练琴系统，让零基础孩子轻松学琴、快乐练琴，彻底粉碎传统方式学琴的痛苦。';
        }
    }
} else if (location.hostname == 'www.yyfc123.com') {
    document.getElementById("icp").innerHTML = '辽ICP备17002816号-6'
    document.title = '音乐蜂巢在线1对1趣味钢琴课，让孩子轻松学琴、快乐练琴'
    var objs = document.getElementsByTagName("meta");
    for (var i = 0; i < objs.length; i++) {
        if (objs[i].name === 'keywords') {
            objs[i].content = '音乐蜂巢,钢琴,电子琴,少儿童孩子学钢琴,钢琴启蒙,钢琴家教,0零基础学钢琴,钢琴陪练,钢琴在线学习,钢琴在线陪练,钢琴视频教学,儿童孩子才艺,怎样学练好钢琴,钢琴陪练软件,钢琴学习app,钢琴线上课哪个好,在家学钢琴,孩子学钢琴app,网上钢琴教学陪练,一对一钢琴教学老师陪练,网上钢琴教学,学钢琴入门到精通,钢琴培训,熊猫陪练 ,熊猫钢琴, 熊猫钢琴陪练, 熊猫陪练, AI陪练,快陪练,小叶子陪练,VIP钢琴陪练,少儿编程,ahaschool,画啦啦,美术宝,vipkid,火花思维';
        }
    }
    for (var i = 0; i < objs.length; i++) {
        if (objs[i].name === 'description') {
            objs[i].content = '音乐蜂巢是专注4-9岁少儿的在线1对1钢琴教学开创者。五阶课程体系由央音、国音、沈音教授、中法两国知名作曲家和台湾儿童心理学者共同倾力研发，学习效果对标音协、英皇考级水准。';
        }
    }
} else if (location.hostname == 'www.ixueqin.cn') {
    document.getElementById("icp").innerHTML = '辽ICP备17002816号-4'
    document.title = '孩子爱学的钢琴课_免费体验_音乐蜂巢-在线1对1趣味钢琴课'
    var objs = document.getElementsByTagName("meta");
    for (var i = 0; i < objs.length; i++) {
        if (objs[i].name === 'keywords') {
            objs[i].content = '音乐蜂巢,钢琴,电子琴,练琴,钢琴才艺,孩子兴趣班,在线学钢琴,钢琴在线陪练 ,幼小衔接,幼升小,琴童,在线学钢琴一对一 ,在线陪练 ,多大学钢琴,怎样练好钢琴,学钢琴的好处,线网上学钢琴陪练 ,学钢琴陪练软件 ,熊猫陪练 ,熊猫钢琴, 熊猫钢琴陪练, 熊猫陪练, AI陪练,快陪练,小叶子陪练,VIP钢琴陪练,少儿编程,ahaschool,画啦啦,美术宝,vipkid,火花思维';
        }
    }
    for (var i = 0; i < objs.length; i++) {
        if (objs[i].name === 'description') {
            objs[i].content = '音乐蜂巢是专注4-9岁少儿的在线1对1钢琴教学开创者。五阶课程体系由央音、国音、沈音教授、中法两国知名作曲家和台湾儿童心理学者共同倾力研发，学习效果对标音协、英皇考级水准。';
        }
    }
} else if (location.hostname == 'www.onlinepiano.cn') {
    document.getElementById("icp").innerHTML = '辽ICP备17002816号-7'
    document.title = '音乐蜂巢在线钢琴_儿童0基础学钢琴_在线学琴陪练考级三合一'
    var objs = document.getElementsByTagName("meta");
    for (var i = 0; i < objs.length; i++) {
        if (objs[i].name === 'keywords') {
            objs[i].content = '音乐蜂巢,钢琴,电子琴,少儿童孩子学钢琴,钢琴启蒙,钢琴家教,0零基础学钢琴,钢琴陪练,钢琴在线学习,钢琴在线陪练,钢琴视频教学,儿童孩子才艺,怎样学练好钢琴,钢琴陪练软件,钢琴学习app,钢琴线上课哪个好,在家学钢琴,孩子学钢琴app,网上钢琴教学陪练,一对一钢琴教学老师陪练,网上钢琴教学,学钢琴入门到精通,钢琴培训,熊猫陪练 ,熊猫钢琴, 熊猫钢琴陪练, 熊猫陪练, AI陪练,快陪练,小叶子陪练,VIP钢琴陪练,少儿编程,ahaschool,画啦啦,美术宝,vipkid,火花思维';
        }
    }
    for (var i = 0; i < objs.length; i++) {
        if (objs[i].name === 'description') {
            objs[i].content = '音乐蜂巢是专注4-9岁少儿的在线1对1钢琴教学开创者。五阶课程体系由央音、国音、沈音教授、中法两国知名作曲家和台湾儿童心理学者共同倾力研发，学习效果对标音协、英皇考级水准。';
        }
    }
} 
// ☝️☝️☝️🐖🐢🐢🐢🐢🐢
function getQueryVariable(variable) {
    var query = window.location.search.substring(1);
    var vars = query.split("&");
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split("=");
        if (pair[0] == variable) {
            return pair[1];
        }
    }
    return (false);
}


var cc1
$(() => {
    /*
    * 👉👀右侧导航center
    */
    let a = ($(window).height() - 278) / 2;
    setInterval(() => {
        let b = ($(window).height() - 278) / 2;
        $('.right-fixed').css('bottom', b + 'px');
    }, 100);
    $('.right-fixed').css('bottom', a + 'px');

    let c = ($(window).height() - 278) / 2;
    setInterval(() => {
        let d = ($(window).height() - 278) / 2;
        $('.right-fixed-popup').css('bottom', d + 'px');
    }, 100);
    $('.right-fixed-popup').css('bottom', c + 'px');

    $('#main').click(() => {
        $('.right-fixed').css('right', '-90px');
    });

    /*
    * 👉👀popup
    * */
    var cc = $('.right-fixed').css('right');

    $('#right-about').mouseover(() => {
        var cc = $('.right-fixed').css('right');
        if (cc == '-90px') {
            $('.right-fixed').css('right', '0px');
            cc1 = $('.right-fixed').css('right');
        }

        if (cc1 == '0px') {
            $('#right-about').mouseover(() => {
                $("#right-fixed-popup").css("display", "block");
                $("#about-popup").css("display", "block");
            });
        }
    });

    $('#right-refer').mouseover(() => {
        var cc = $('.right-fixed').css('right');
        if (cc == '-90px') {
            $('.right-fixed').css('right', '0px');
            cc1 = $('.right-fixed').css('right');
        }

        if (cc1 == '0px') {
            // $('#right-about').mouseover(() => {
            //     $("#right-fixed-popup").css("display", "block");
            //     $("#about-popup").css("display", "block");
            // });
        }
    });

    $('#right-download').mouseover(() => {
        var cc = $('.right-fixed').css('right');
        if (cc == '-90px') {
            $('.right-fixed').css('right', '0px');
            cc1 = $('.right-fixed').css('right');
        }
        if (cc1 == '0px') {
            $('#right-download').mouseover(() => {
                $("#right-fixed-popup").css("display", "block");
                $("#download-popup").css("display", "block");
            });
        }
    });

    $('#right-phone').mouseover(() => {
        var cc = $('.right-fixed').css('right');
        if (cc == '-90px') {
            $('.right-fixed').css('right', '0px');
            cc1 = $('.right-fixed').css('right');
        }
        if (cc1 == '0px') {
            $('#right-phone').mouseover(() => {
                $("#right-fixed-popup").css("display", "block");
                $("#phone-popup").css("display", "block");
            });
        }
    });

    $('#right-about').mouseout(() => {
        $("#right-fixed-popup").css("display", "none");
        $("#about-popup").css("display", "none");
    });

    $('#right-download').mouseout(() => {
        $("#right-fixed-popup").css("display", "none");
        $("#download-popup").css("display", "none");
    });

    $('#right-phone').mouseout(() => {
        $("#right-fixed-popup").css("display", "none");
        $("#phone-popup").css("display", "none");
    });

    /*
    * 🦞领取弹窗
    * */
    $('#btn-draw').click(() => {
        $(".floating-window").css("display", "block");
    });

    $('.growth-system-draw').click(() => {
        $(".floating-window").css("display", "block");
    });

    $('#close-btn').click(() => {
        $(".floating-window").css("display", "none");
    });

    $('.succeed-close').click(() => {
        $(".succeed").css("display", "none");
    });

    $("#cctv").click(function (eve) {
        window.location = 'about.html#yfx'
    });

    // toast 消息
    function toastMsg(msg) {
        $("#msg").text(msg);
        setTimeout(function () {
            $("#msg").text('');
        }, 2000);
    }



    /*
    * 💼收集信息
    * */
    $('#bottom-btn').click(() => {
        var zhi = getQueryVariable('act_qd_str'); 
        if (zhi == '' || zhi == false || zhi == undefined) {
            zhi = 'pc官网';
        }
        var age_bottom = $("#bottom-age").val();
        if (age_bottom.length == 0) {
            $("#age-ball").css("display", "block");
            setTimeout(() => {
                $("#age-ball").css("display", "none");
            }, 2000)
            return;
        }
        var data = new Date().getFullYear();
        var baby_birthday = data - age_bottom + '-01-01';
  
        var mobile_bottom = $("#bottom-mobile").val();  
        if(isHaiwai) mobile_bottom=86;
        if (mobile_bottom.length == 0) {
            $("#phone-ball").css("display", "block");
            setTimeout(() => {
                $("#phone-ball").css("display", "none");
            }, 2000)
            return;
        }
        var pattern = /^\d{1,20}$/;
        if (!isHaiwai&&!pattern.test(mobile_bottom)) {
            $("#yes-phone-ball").css("display", "block");
            setTimeout(() => {
                $("#yes-phone-ball").css("display", "none");
            }, 2000)
            return;
        }
        var wx_id_bottom = $("#bottom-wx_id").val();
        wx_id_bottom = wx_id_bottom.trim();
        if (isHaiwai){
            if(wx_id_bottom.length == 0) {  
                $("#bottom-wx_id").val(''); 
                return;
             }else{
                mobile_bottom=86;
             }
        }

        $.ajax({
            url: 'https://web.yinyuefengchao.com/stu/website/collectionUserPcFromWebsite.do',
            data: {
                stu_mobile: mobile_bottom,
                stu_birthday: baby_birthday,
                source_type: 1,
                act_qd_str: zhi,
                wx_id:wx_id_bottom
            },
            type: 'GET',
            dataType: 'json',
            success: function (res) {
                // console.log('接口调用信息:', res);
                $(".succeed").css("display", "block");
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) { 
                alert("您好，目前系统正在维护中，暂时无法查看，请谅解。");

            }
        });

    });

    var width = $(window).width();
    // console.log(width)
    var age_ball = (width - 736) / 2 + 'px';

    $('.stu-duoduo').css('margin-left', age_ball);
    setInterval(() => {
        var width1 = $(window).width();
        var age_ball1 = (width1 - 736) / 2 + 'px';
        $('.stu-duoduo').css('margin-left', age_ball1);
    }, 100);

    $('#music_form_button').click(() => {
        var zhi = getQueryVariable('act_qd_str'); 
        if (zhi == '' || zhi == false || zhi == undefined) {
            zhi = 'pc官网';
        }
        var music_form_age = $("#music_form_age").val();
        if (music_form_age.length == 0) {
            toastMsg('请填写宝贝的年龄')
            return;
        }
        var data = new Date().getFullYear()
        var baby_birthday = data - music_form_age + '-01-01';
        var music_form_mobile = $("#music_form_mobile").val();
        music_form_mobile = music_form_mobile.trim();
        if(isHaiwai) music_form_mobile=86;
        if (music_form_mobile.length == 0) {
            toastMsg('请输入您的手机号');
            return;
        }

        var pattern = /^\d{1,20}$/;
        if (!isHaiwai&&!pattern.test(music_form_mobile)) { 
            toastMsg('请输入正确的手机号');
            return;
        }
        // var floating_window_wx_id= $("#floating-window-wx_id").val();
        // floating_window_wx_id = floating_window_wx_id.trim();
        // if (isHaiwai){
        //     if (floating_window_wx_id.length == 0) {
        //         toastMsg('请输入微信号或email');
        //         return;
        //     }else{
        //         floating_window_mobile=86;
        //     }
        // }
        
        $("#music_form_age").val('')
        $("#music_form_mobile").val('')
        $("#floating-window-wx_id").val('')
        // var script = document.createElement("script");
        // var url = "/stu/website/collectionUserPcFromWebsite.do?stu_mobile="+floating_window_mobile+"&stu_birthday="+baby_birthday+'&source_type=1&act_qd_str=zhi&wx_id='+floating_window_wx_id+'&func=handleResponse'
        // script.setAttribute('src',url);
        // script.setAttribute('type','text/javascript');
        // document.getElementsByTagName('head')[0].appendChild(script)
        // function handleResponse(obj){
        //     console.log(obj.message)
        // }

        $.ajax({
            url: 'https://web.yinyuefengchao.com/stu/website/collectionUserPcFromWebsite.do',
            data: {
                stu_mobile: music_form_mobile,
                stu_birthday: baby_birthday,
                source_type: 1,
                act_qd_str: zhi,
                wx_id: null
            },
            async : true,
            type: 'GET',
            // dataType: 'json',
            dataType : "json",
            success: function (res) { 
                $(".succeed").css("display", "block");
                $(".floating-window").css("display", "none");
                $("#music_form_age").val('')
                $("#music_form_mobile").val('')
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                // console.log(XMLHttpRequest.status);
                // console.log(XMLHttpRequest.readyState);
                // console.log(textStatus);
                alert("您好，目前系统正在维护中，暂时无法查看，请谅解。");

            }
        });
    })

    $('#floating-window-btn').click(() => {
        var zhi = getQueryVariable('act_qd_str'); 
        if (zhi == '' || zhi == false || zhi == undefined) {
            zhi = 'pc官网';
        }
        var floating_window_age = $("#floating-window-age").val();
        if (floating_window_age.length == 0) {
            toastMsg('请选择宝贝的年龄')
            return;
        }
        var data = new Date().getFullYear()
        var baby_birthday = data - floating_window_age + '-01-01';
        var floating_window_mobile = $("#floating-window-mobile").val();
        floating_window_mobile = floating_window_mobile.trim();
        if(isHaiwai) floating_window_mobile=86;
        if (floating_window_mobile.length == 0) {
            toastMsg('请输入您的手机号');
            return;
        }

        var pattern = /^\d{1,20}$/;
        if (!isHaiwai&&!pattern.test(floating_window_mobile)) { 
            toastMsg('请输入正确的手机号');
            return;
        }
        var floating_window_wx_id= $("#floating-window-wx_id").val();
        floating_window_wx_id = floating_window_wx_id.trim();
        if (isHaiwai){
            if (floating_window_wx_id.length == 0) {
                toastMsg('请输入微信号或email');
                return;
            }else{
                floating_window_mobile=86;
            }
        }
        
        $("#floating-window-age").val('')
        $("#floating-window-mobile").val('')
        $("#floating-window-wx_id").val('')
        // var script = document.createElement("script");
        // var url = "/stu/website/collectionUserPcFromWebsite.do?stu_mobile="+floating_window_mobile+"&stu_birthday="+baby_birthday+'&source_type=1&act_qd_str=zhi&wx_id='+floating_window_wx_id+'&func=handleResponse'
        // script.setAttribute('src',url);
        // script.setAttribute('type','text/javascript');
        // document.getElementsByTagName('head')[0].appendChild(script)
        // function handleResponse(obj){
        //     console.log(obj.message)
        // }

        $.ajax({
            url: 'https://web.yinyuefengchao.com/stu/website/collectionUserPcFromWebsite.do',
            data: {
                stu_mobile: floating_window_mobile,
                stu_birthday: baby_birthday,
                source_type: 1,
                act_qd_str: zhi,
                wx_id: floating_window_wx_id
            },
            async : true,
            type: 'GET',
            // dataType: 'json',
            dataType : "json",
            success: function (res) { 
                $(".succeed").css("display", "block");
                $(".floating-window").css("display", "none");
                $("#floating-window-age").val('')
                $("#floating-window-mobile").val('')
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                // console.log(XMLHttpRequest.status);
                // console.log(XMLHttpRequest.readyState);
                // console.log(textStatus);
                alert("您好，目前系统正在维护中，暂时无法查看，请谅解。");

            }
        });
    })
   
    var bottomMobile = document.querySelector("#bottom-mobile");
    var floatingMobile = document.querySelector("#floating-window-mobile");
    var bottomWx_id = document.querySelector("#bottom-wx_id");
    var floatingWx_id = document.querySelector("#floating-window-wx_id");
 
    if(isHaiwai){ 
        bottomMobile.style.display = 'none';
        floatingMobile.style.display = 'none';
        bottomWx_id.style.display = '';
        floatingWx_id.style.display = '';
    }

 
});

/*
* 🧚‍♀️轮播
* */
window.init();

function init() {
    let i = 1;
    // setInterval(function changeImg() {
    //     i++;
    //     document.getElementById("carouse").src = "https://mh-resource.yinyuefengchao.com/www/WeChat" + i + ".png";
    //     if (i == 2) {
    //         i = 0;
    //     }
    // }, 3000);
}
new Vue({
    el: '#nav',
    data() {
        return {
            curIndex: 0,
            scrollFirst: true,
            nav: [
                '首页','课程体系','教研专家','师资建设','学琴干货','联系我们'
            ],
        }
    },
    mounted() {
        window.addEventListener('scroll', this.handleScroll, true)
    },
    methods: {
        handleScroll:  function () {
            var top = Math.floor($(".curriculum-structure-page").offset().top);//获取导航栏变色的位置距顶部的高度
            var top1 = Math.floor($("#expert-page").offset().top);//获取导航栏变色的位置距顶部的高度
            var top2 = Math.floor($("#teacher-page").offset().top);//获取导航栏变色的位置距顶部的高度
            var top3 = Math.floor($("#bottom-content").offset().top);//获取导航栏变色的位置距顶部的高度
            var scrollTop = $(window).scrollTop();//获取当前窗口距顶部的高度
            if (scrollTop > 150) {
                this.scrollFirst = false
            }else {
                this.scrollFirst = true
            }
            if (scrollTop < top) {
                this.curIndex = 0
            }
            if (scrollTop >= top && scrollTop < top1) {
                this.curIndex = 1
            } else if (scrollTop >= top1 && scrollTop < top2) {
                this.curIndex = 2
            } else if (scrollTop >= top2 && scrollTop < top3) {
                this.curIndex = 3
            }
        },
        changeSlide: function (index) {
            this.curIndex = index
            if(index==0){
                this.scrollFirst = true
            }else {
                this.scrollFirst = false
            }
            var url
            switch (index) {
                case 0:
                    url = 'index.html#home-page'
                    break;
                case 1:
                    url = 'index.html#curriculum-structure-page'
                     break;
                case 2:
                    url = 'index.html#expert-page'
                     break;
                case 3:
                    this.curIndex = 3
                    url = 'index.html#teacher-page'
                     break;
                case 4:
                    this.curIndex = 4
                    url = 'article.html'
                     break;
                case 5:
                    this.curIndex = 5
                    url = 'about.html'
            }
            window.location = url
        }
    },
});

new Vue({
    el: '#video_box',
    data() {
        return {
            textAll1: false,
            textAll2: false,
            textAll3: false,
        }
    },
    mounted() {
    },
    methods: {
        showAll(index){
            console.log(index)
            if(index==1){
                this.textAll1 = true
            }else if(index==2) {
                this.textAll2 = true
            }else if(index==3) {
                this.textAll3 = true
            }
        },
        hideAll(index){
            if(index==1){
                this.textAll1 = false
            }else if(index==2) {
                this.textAll2 = false
            }else if(index==3) {
                this.textAll3 = false
            }
        }
    },
});

new Vue({
    el: '#expert-page',
    data() {
        return {
            img: [
                { id: 0, url: 'https://mh-resource.yinyuefengchao.com/www2001/xiaobo.png' },
                { id: 1, url: 'https://mh-resource.yinyuefengchao.com/www2001/zhangfang.png' },
                { id: 2, url: 'https://mh-resource.yinyuefengchao.com/www2001/zhangwei.png' },
                { id: 3, url: 'https://mh-resource.yinyuefengchao.com/www2001/lqs.png' },
                { id: 4, url: 'https://mh-resource.yinyuefengchao.com/www2001/qb.png' }
            ]
        }
    },
    beforeMount: function () {
        this.width = $(window).width() / 3 + 'px';
    },
});

new Vue({
    el: '#traditional-teaching',
    data() {
        return {
            img: [
                { id: 0, url: 'https://mh-resource.yinyuefengchao.com/www2001/tuition-fee1.png' },
                { id: 1, url: 'https://mh-resource.yinyuefengchao.com/www2001/tuition-fee2.png' },
                { id: 2, url: 'https://mh-resource.yinyuefengchao.com/www2001/tuition-fee3.png' },
                { id: 3, url: 'https://mh-resource.yinyuefengchao.com/www2001/tuition-fee4.png' },
                { id: 4, url: 'https://mh-resource.yinyuefengchao.com/www2001/tuition-fee5.png' },
                { id: 5, url: 'https://mh-resource.yinyuefengchao.com/www2001/tuition-fee6.png' },
            ]
        }
    }
});

new Vue({
    el: '#bottom-navigation',
    data() {
        return {
            options: [{
                value: '选项1',
                label: '4'
            }, {
                value: '选项2',
                label: '5'
            }, {
                value: '选项3',
                label: '6'
            }, {
                value: '选项4',
                label: '7'
            }, {
                value: '选项5',
                label: '8'
            }, {
                value: '选项6',
                label: '9'
            }, {
                value: '选项7',
                label: '10'
            }, {
                value: '选项8',
                label: '11'
            }, {
                value: '选项9',
                label: '12'
            }],
            value: ''
        }
    }
});


new Vue({
    el: '#floating-window',
    data() {
        return {
            options: [{
                value: '选项1',
                label: '4'
            }, {
                value: '选项2',
                label: '5'
            }, {
                value: '选项3',
                label: '6'
            }, {
                value: '选项4',
                label: '7'
            }, {
                value: '选项5',
                label: '8'
            }, {
                value: '选项6',
                label: '9'
            }, {
                value: '选项7',
                label: '10'
            }, {
                value: '选项8',
                label: '11'
            }, {
                value: '选项9',
                label: '12'
            }],
            value: ''
        }
    }
});

//🧚‍♀️百度统计
var _hmt = _hmt || [];
(function () {
    var hm = document.createElement("script");
    hm.src = "https://hm.baidu.com/hm.js?fe73c194f253c1ce47b19f06dfe66f65";
    var s = document.getElementsByTagName("script")[0];
    s.parentNode.insertBefore(hm, s);
})();
